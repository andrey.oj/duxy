import {
  EXAMPLE_DO_DATA,
  EXAMPLE_DO_DATA_DONE,
} from './exampleTypes';

export const exampleDoData = () => ({
  type: EXAMPLE_DO_DATA,
});
export const exampleDoDataDone = data => ({
  type: EXAMPLE_DO_DATA_DONE,
  payload: data,
});
