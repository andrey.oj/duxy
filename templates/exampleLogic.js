import { createLogic } from 'redux-logic';
import fetch from 'node-fetch';
import {
    EXAMPLE_DO_DATA,
} from './exampleTypes';
import { exampleDoDataDone } from './exampleActions';

const fetchDataLogic = createLogic({
    type: EXAMPLE_DO_DATA,
    latest: true,

    process({ example, action }, dispatch, done) {
        const data = action.payload;
        dispatch(exampleDoDataDone(data));
        done();
    },
});


export default [fetchDataLogic];
