import {
  EXAMPLE_DO_DATA,
  EXAMPLE_DO_DATA_DONE,
} from './exampleTypes';

export default function exampleReducer(
    state = {
        test: 'test',
    },
    action,
) {
    switch (action.type) {
        case EXAMPLE_DO_DATA:
            return {
                ...state,
                test: 'Doing',
            };
        case EXAMPLE_DO_DATA_DONE:
            return {
                ...state,
                ...action.payload,
                test: 'done',
            };

        default:
            return state;
    }
}
