import React from 'react';
import { connect } from 'react-redux';

import { exampleDoData } from '../redux/exampleActions';

const ExampleComponent = props => (
  <div>
      Component example
{' '}
{props.test}
  </div>
);

const mapStateToProps = state => ({
    test: state.example.test,
});

const mapDispatchToProps = dispatch => ({
  exampleDoData: () => dispatch(exampleDoData()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ExampleComponent);
