var path = require('path');

module.exports = () => (
  [
    {
      mode: 'development',
      output: {
        filename: 'start.js',
        path: path.join(__dirname, 'dist'), // where to place webpack files
      },
      entry: {
        start: './start.js',
      },
      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: [/node_modules/, /(build)/,/(dist)/],
            use: {
              loader: 'babel-loader',
            },
          },

        ],
      },

      target: 'node',
      node: {
        __dirname: false,
        __filename: false,
      }
    },
  ]
);