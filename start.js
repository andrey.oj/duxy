const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const targz = require('targz');
const createNewAction = require('./src/creators/createNewAction');
const createNewFlow = require('./src/creators/createNewFlow');
const readConfig = require('./src/functions/readConfig');
const globals = require('./src/globals');

const { commands, printHelp } = globals;
handleRequest();

function printDone({ success, error = false }) {
  if (success) {
    console.log(chalk.yellow('Finish '), chalk.green('SUCCESS'));
  } else {
    console.log(chalk.yellow('Finish '), chalk.red('FAIL'), error ? chalk.magenta(error) : '');
  }

  return success;
}

function checkInputAndFiles() {
  return new Promise((resolve, reject) => {
    if (process.argv.length < 3 || process.argv[2] === 'help') {
      printHelp(commands);
      reject();

    }
    else {

      //  read parameters
      const command = process.argv[2];
      const name = process.argv[3] || '';
      const args = process.argv.length > 4 ? process.argv.splice(4,4) : [];
      const launchDir = process.env.PWD;

      const templatesDir = path.join(__dirname, 'templates');

      console.info(chalk.yellow('ARGS'), chalk.grey(args.join()));
      //console.info(chalk.yellow('LAUNCH'), chalk.grey(launchDir));
      //console.info(chalk.yellow('ROOT'), chalk.grey(root));

      let directories  = {
        sourceHome: {
          homeDir: launchDir,
          srcDir: path.join(launchDir, 'src'),
          templatesDir
        },
        destHome: {
          homeDir: launchDir,
          srcDir: path.join(launchDir, 'src')
        },
        launchDir
      };

      //  Scan for directories
      const { sourceHome,destHome } = directories;

      // Check if template exist
      const templExist = fs.existsSync(templatesDir);
      const tarFile = path.resolve(__dirname,'templates.tar.gz');
      const nodeTar = path.join(sourceHome.homeDir, 'node_modules', 'rdxcreate','templates.tar.gz');


      if(!templExist ){
        console.warn('templates not found', templatesDir);
        if( fs.existsSync(tarFile)){
          decompressTemplates( __dirname,tarFile ).then(ok=>{
            if(ok) resolve({ command,name,sourceHome, destHome,args });
            else reject('Could not decompress');
          });
        }else if( fs.existsSync(nodeTar)){
          decompressTemplates(__dirname, nodeTar).then(ok=>{
            if(ok) resolve({ command,name,sourceHome, destHome,args });
            else reject('Could not decompress');
          });
        }
        else reject('Tar not found: '+ tarFile);

      }
      else {
        if(templExist) resolve({ command,name,sourceHome, destHome });
        else reject('Templates not found');
      }

    }
  });
}

function run() {
  //  Check if enough parameters
  return new Promise(resolve => {

    checkInputAndFiles()
      .then(({ command,name,sourceHome, destHome,args }) => {

      //  Read config file
      const config = readConfig(destHome.homeDir);

      try {
        switch (command) {

          case commands.CreateNewFlow.name:

            createNewFlow(name, sourceHome, destHome, config)
              .then(success => {
                resolve({ success });
              })
              .catch(error => {
                resolve({ success: false, error });
              });
            break;

          case commands.CreateNewAction.name:

            createNewAction(name, destHome, config)
              .then(success => {
                resolve({ success });
              })
              .catch(error => {
                resolve({ success: false, error });
              });
            break;

          default:
            resolve({ success: false, error: `Command not found: ${command}` });
            break;
        }
      } catch (error) {
        resolve({ success: false, error });
      }

    }).catch((error)=>{
      resolve({ success: false, error });
    });


  });
}



function decompressTemplates(dest, src  = 'templates.tar.gz') {
  return new Promise(resolve => {

    if(!fs.existsSync(dest)) fs.mkdirSync(dest);
    targz.decompress({ src, dest, }, (err) => {
      if(err) {
        console.log(err);
        resolve(false);
      } else {
        console.log(chalk.green("Files decompressed!"));
        resolve(true);
      }
    });
  });

}

function handleRequest() {
  run().then(result => {
    printDone(result);
  });
}
