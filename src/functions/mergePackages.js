const fs = require('fs');
const chalk = require('chalk');

module.exports = (master, slave) => {
    try {
        const origin = JSON.parse(fs.readFileSync(master, 'utf8'));
        const copy = JSON.parse(fs.readFileSync(slave, 'utf8'));

        const merged = {
            ...copy,
            ...origin,

            dependencies: {
                ...origin.dependencies,
                ...copy.dependencies
            },
            devDependencies: origin.devDependencies,

            scripts: {
                ...origin.scripts,
                'new-flow': 'node node_modules/rdxcreate new'
            }
        };

        return JSON.stringify(merged);
    } catch (e) {
        console.log(chalk.red('Error merging packages'));
        return JSON.stringify(master);
    }
};
