const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const readConfig = require('./readConfig');

const CONF_NAME = 'rc.config.json';

module.exports = homeDir => {
    const p = path.join(homeDir, CONF_NAME);
    try {
        const data = JSON.stringify(readConfig(), null, 2);
        fs.writeFileSync(p, data);
        console.info(chalk.yellow(`Config created:`, chalk.blue(CONF_NAME)));
        return true;
    } catch (e) {
        console.error(chalk.red(`Could not create config in `), chalk.blue(homeDir), e.error);
        return false;
    }
};
