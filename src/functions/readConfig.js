const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const { hierarchyTypes } = require('../globals');

const defaultConfig = {
    srcDir: 'src',
    componentsDir: 'pages',
    reduxDir: 'redux',
    configStoreFile: 'configureStore.js',
    rootReducerFile: 'rootReducer.js',
    rootLogicFile: 'rootLogic.js',

    hierarchy: hierarchyTypes.Component.name,
    hierarchyTree: {}
};

function createConfig(config) {
    if (config) {
        return {
            ...defaultConfig,
            ...config
            // hierarchyTree: getHierarchy(config)
        };
    }
    return {
        ...defaultConfig
        // hierarchyTree: getHierarchy(defaultConfig)
    };
}

module.exports = homeDir => {
    if (!homeDir) {
        return defaultConfig;
    }

    const p = path.join(homeDir, 'rc.config.json');
    try {
        const lines = fs.readFileSync(p, 'utf8');
        if (!lines) {
            return createConfig();
        }
        const conf = JSON.parse(lines);

        console.log(chalk.yellow('Config found'));
        return createConfig(conf);
    } catch (e) {
        console.log(chalk.yellow('Using default config'));
        return createConfig();
    }
};
