const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const readline = require('readline');
const createConfig = require('../functions/createConfig');
const mergePackages = require('../functions/mergePackages');

function copyFileSync(source, target) {
    let targetFile = target;

    // if target is a directory a new file with the same name will be created
    if (fs.existsSync(target)) {
        if (fs.lstatSync(target).isDirectory()) {
            targetFile = path.join(target, path.basename(source));
        }
    }

    fs.writeFileSync(targetFile, fs.readFileSync(source));
}

function copyFolderRecursiveSync(source, target) {
    let files = [];

    // check if folder needs to be created or integrated
    const targetFolder = path.join(target, path.basename(source));
    if (!fs.existsSync(targetFolder)) {
        fs.mkdirSync(targetFolder);
    }

    // copy
    if (fs.lstatSync(source).isDirectory()) {
        files = fs.readdirSync(source);
        files.forEach(file => {
            const curSource = path.join(source, file);
            if (fs.lstatSync(curSource).isDirectory()) {
                copyFolderRecursiveSync(curSource, targetFolder);
            } else {
                copyFileSync(curSource, targetFolder);
            }
        });
    }
}

function continieCopyFiles(
    resolve,
    reject,
    { name, config, myHome, homeDir, srcDir, templatesDir, destPkg, srcPkg }
) {
    const pages = `${srcDir}/${config.componentsDir}/`;
    const redux = `${srcDir}/${config.reduxDir}/`;

    if (!fs.existsSync(pages)) fs.mkdirSync(pages);
    if (!fs.existsSync(redux)) fs.mkdirSync(redux);

    const templHome = path.join(templatesDir, 'home');
    const templSrc = path.join(templatesDir, 'src');
    const templPublic = path.join(templatesDir, 'public');

    console.info(
        chalk.yellow('Copying home files from '),
        chalk.blue(templatesDir),
        chalk.yellow('to'),
        chalk.blue(homeDir)
    );

    try {
        if (!createConfig(homeDir)) {
            console.error(chalk.red('Config not created'));
            reject('Config not created');
            return;
        }
        fs.copyFileSync(path.join(templHome, '.babelrc'), path.join(homeDir, '.babelrc'));
        fs.copyFileSync(path.join(templHome, '.eslintignore'), path.join(myHome, '.eslintignore'));
        fs.copyFileSync(path.join(templHome, '.eslintrc.json'), path.join(myHome, '.eslintrc.json'));
        fs.copyFileSync(path.join(templHome, 'gitignore'), path.join(homeDir, '.gitignore'));
        fs.copyFileSync(path.join(templHome, 'README.md'), path.join(homeDir, 'README.md'));
        fs.copyFileSync(path.join(templHome, 'webpack.config.js'), path.join(homeDir, 'webpack.config.js'));

        copyFolderRecursiveSync(templSrc, homeDir);
        copyFolderRecursiveSync(templPublic, homeDir);

        // Copy package.json

        if (destPkg) {
            //  master - slave
            const pkgData = mergePackages(destPkg, srcPkg);
            fs.open(destPkg, 'w', (err, fd) => {
                if (err) reject(err);
                fs.writeFile(fd, pkgData, e => {
                    if (e) reject(err);
                    console.log(chalk.yellow('Merged'), chalk.blue('package.json'));
                    installDeps(resolve);
                });
            });
        } else {
            fs.copyFileSync(srcPkg, path.join(homeDir, 'package.json'));
            installDeps(resolve);
        }
    } catch (err) {
        console.info(chalk.red('Error occured while copying'), err);
        reject('Error occurred while copying:' + err.message);
    }
}

function installDeps(resolve) {
    console.log(chalk.magenta('Go to your folder and type'), chalk.green('npm install'));
    console.log(chalk.magenta('After that run your dev server with '), chalk.green('npm run dev'));
    resolve(true);

    /*
    const command = 'npm';
    const args = ['install'];

    console.log(chalk.magenta('Installing dependencies.....'));
    const child = spawn(command, args, { stdio: 'inherit' });
    child.on('close', code => {
        if (code !== 0) {
            resolve({ success: true, error: `${command} ${args.join(' ')}` });
        }
        resolve({ success: true });
    });
    */
}

module.exports = (name, source, dest, config) => {
    return new Promise((resolve, reject) => {
        const { homeDir, srcDir } = dest;
        const myHome = source.homeDir;
        const { templatesDir } = source;
        const srcPkg = path.join(templatesDir, 'home', 'package.json');
        const destPkg = path.join(homeDir, 'package.json');

        if (fs.existsSync(homeDir)) {
            console.warn(chalk.magenta('Home directory already exists'));
        } else {
            fs.mkdirSync(homeDir);
        }

        if (fs.existsSync(destPkg)) {
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });

            //console.warn(chalk.red('src directory already exists, overwrite?'));
            rl.question(chalk.red('package.json exists, merge ? [yes/no]'), answer => {
                if (answer === 'yes') {
                    continieCopyFiles(resolve, reject, {
                        name,
                        config,
                        myHome,
                        homeDir,
                        srcDir,
                        templatesDir,
                        destPkg,
                        srcPkg
                    });
                }
                rl.close();
            });
        } else {
            fs.mkdirSync(srcDir);
            continieCopyFiles(resolve, reject, {
                name,
                config,
                myHome,
                homeDir,
                srcDir,
                templatesDir,
                destPkg: null,
                srcPkg
            });
        }
    });
};
