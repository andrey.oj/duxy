const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

function addLogic(rootFile, destDir, name) {
    const lines = fs.readFileSync(rootFile, 'utf8').split('\n');
    const objName = `${name}Logic`;
    let importDone = false;
    for (let i = 0; i < lines.length; i += 1) {
        const line = lines[i];
        if (line.indexOf(`import ${name}Logic from`) >= 0) {
            console.log(chalk.magenta(`Logic already taken:${name}`));
            return false;
        }

        if (line.indexOf('import') < 0 && !importDone) {
            lines.splice(i, 0, [`import ${name}Logic from '../pages/${name}/redux/${objName}';`]);
            importDone = true;
        }

        if (line.indexOf('export') >= 0) {
            const items = lines.slice(i);

            for (let x = 0; x < items.length; x += 1) {
                const indx = items[x].indexOf('...');
                if (indx >= 0) {
                    lines[i + x] = `${items[x].slice(0, indx)}...${name}Logic,\n${items[x].slice(indx)}`;
                    break;
                }
            }
        }
    }
    const data = lines.join('\n');

    try {
        fs.writeFileSync(path.join(destDir, 'rootLogic.js'), data);
        console.info(chalk.yellow('rootLogic updated'));
        return true;
    } catch (err) {
        /* Handle the error */
        console.info(chalk.magenta('Could not update root logic'));
        return false;
    }
}

function addReducer(rootFile, destDir, name) {
    const lines = fs.readFileSync(rootFile, 'utf8').split('\n');
    const objName = `${name}Reducer`;
    let importDone = false;
    for (let i = 0; i < lines.length; i += 1) {
        const line = lines[i];
        if (line.indexOf(`import ${name} from`) >= 0) {
            console.log(chalk.magenta(`Reducer already taken:${name}`));
            return false;
        }

        if (line.indexOf('import') < 0 && !importDone) {
            lines.splice(i, 0, [`import ${name} from '../pages/${name}/redux/${objName}';`]);
            importDone = true;
        }

        if (importDone && line.indexOf('combineReducers') >= 0) {
            lines[i + 1] += `\n\t${name},`;
            break;
        }
    }

    const data = lines.join('\n');

    try {
        fs.writeFileSync(path.join(destDir, 'rootReducer.js'), data);
        console.info(chalk.yellow('rootReducer updated'));
    } catch (err) {
        /* Handle the error */
        console.info(chalk.magenta('Could not update root reducer'));
        return false;
    }
    return true;
}

function writePage(src, dir, name, file, appendName = true) {
    return new Promise((resolve, reject) => {
        const data = fs
            .readFileSync(path.join(src, `example${file}`), 'utf8')
            .replace(/example/g, name)
            .replace(/Example/g, name.slice(0, 1).toUpperCase() + name.slice(1, name.length))
            .replace(/EXAMPLE/g, name.toUpperCase());

        const destination = path.join(dir, `${appendName ? name : ''}${file}`);
        fs.open(destination, 'wx', (err, fd) => {
            if (err) reject(err);
            fs.writeFile(fd, data, (e, w) => {
                if (e) reject(err);
                console.log(chalk.yellow('Created file:'), chalk.grey(path.basename(destination)));
                resolve(w);
            });
        });
    });
}

module.exports = (name, sourceHome, destHome, config) => {
    const { srcDir, reduxDir, componentsDir, rootLogicFile, rootReducerFile } = config;

    console.info(chalk.yellow('Searching directories..'));
    const { homeDir, templatesDir } = sourceHome;

    const destSrc = path.join(destHome.homeDir, srcDir);

    const compDir = path.join(destSrc, componentsDir);
    const rootDir = path.join(compDir, name);

    const destComponents = path.join(rootDir, 'components');
    const destRedux = path.join(rootDir, 'redux');

    const srcReduxFile = path.join(templatesDir, srcDir, reduxDir, rootReducerFile);
    const srcLogicFile = path.join(templatesDir, srcDir, reduxDir, rootLogicFile);

    const destReduxDir = path.join(destSrc, reduxDir);
    const destLogicDir = path.join(destSrc, reduxDir);

    return new Promise((resolve, reject) => {
        try {
            if (!fs.existsSync(destSrc)) {
                reject('No source dir found');
                console.info(chalk.magenta('No source dir'), destSrc);
                return;
            }
        } catch (e) {
            console.info(chalk.red('No source dir'));
        }

        const pages = fs.readdirSync(compDir).filter(file => file.indexOf('.') < 0);

        if (pages.indexOf(name) >= 0) {
            reject('Dir already exists');
            return;
        }

        console.info(chalk.yellow('Creating directories..'));
        //  Add folder structure
        fs.mkdirSync(rootDir);
        fs.mkdirSync(destComponents);
        fs.mkdirSync(destRedux);

        Promise.all([
            //  Copy Reducer stuff
            writePage(templatesDir, destRedux, name, 'Actions.js'),
            writePage(templatesDir, destRedux, name, 'Logic.js'),
            writePage(templatesDir, destRedux, name, 'Reducer.js'),
            writePage(templatesDir, destRedux, name, 'Types.js'),
            // Copy exmaple coponent
            writePage(templatesDir, destComponents, name, 'Component.js', false)
        ]).then(() => {
            console.info(chalk.yellow('Files created and copied'));

            addLogic(srcLogicFile, destLogicDir, name);
            addReducer(srcReduxFile, destReduxDir, name);

            console.log(`Workflow created:`, chalk.cyan(name));
            resolve(true);
        });
    });
};
