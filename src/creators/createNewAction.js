const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

function insertType(input, { objName }) {
    for (let i = 0; i < input.length; i += 1) {
        if (i === input.length - 1) {
            input[i] += `\nexport const ${objName} = '${objName}';`;
        }
    }

    return input.join('\n');
}

function insertAction(input, { objName, functionName }) {
    let importDone = false;
    for (let i = 0; i < input.length; i += 1) {
        const line = input[i];
        if (line.indexOf('import {') >= 0 && !importDone) {
            input[i] += line.substring(0, 8) + `\n${objName},` + line.substring(8, line.length);
            importDone = true;
        }

        if (i === input.length - 1) {
            input[i] += `\nexport const ${functionName} = () => ({
  type: ${objName},
  payload: null
});`;
        }
    }

    return input.join('\n');
}

function insertReducer(input, { objName }) {
    let importDone = false;

    for (let i = 0; i < input.length; i += 1) {
        const line = input[i];
        if (line.indexOf('import {') >= 0 && !importDone) {
            input[i] += `\n${objName},`;
            importDone = true;
        }
        if (line.indexOf('switch (action.type)') >= 0) {
            input[i] += `\ncase ${objName}:
            return {
                ...state,
            };`;
            break;
        }
    }

    return input.join('\n');
}

module.exports = (name, destHome, config) => {
    const { srcDir, reduxDir, componentsDir } = config;

    const { homeDir } = destHome;
    return new Promise((resolve, reject) => {
        const args = name.split('_');
        const type = args[0];
        args.splice(0, 1);
        const actionName = args.join('_');

        const destComp = path.join(homeDir, srcDir, componentsDir, type);
        if (!fs.existsSync(destComp)) {
            reject('Component not found: ' + destComp);
            return;
        }
        const objName = `${type.toUpperCase()}_${actionName.toUpperCase()}`;
        const functionName = `${type.toLowerCase()}_${actionName
            .split('_')
            .map(n => n.substring(0, 1).toUpperCase() + n.substring(1, n.length).toLowerCase())
            .join('')}`;

        const destTypesFile = path.join(destComp, reduxDir, `${type}Types.js`);
        const destActionFile = path.join(destComp, reduxDir, `${type}Actions.js`);
        const destReducerFile = path.join(destComp, reduxDir, `${type}Reducer.js`);

        let lines;
        try {
            lines = fs.readFileSync(destTypesFile, 'utf8').split('\n');
            const typeData = insertType(lines, { objName });
            fs.writeFileSync(destTypesFile, typeData);

            lines = fs.readFileSync(destActionFile, 'utf8').split('\n');
            const actionData = insertAction(lines, { objName, functionName });
            fs.writeFileSync(destActionFile, actionData);

            lines = fs.readFileSync(destReducerFile, 'utf8').split('\n');
            const reducerData = insertReducer(lines, { objName });
            fs.writeFileSync(destReducerFile, reducerData);

            resolve(true);
        } catch (e) {
            reject(e);
        }
    });
};
