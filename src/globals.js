const chalk = require('chalk');

module.exports = {
    commands: {
        CreateNewProject: {
            name: 'init',
            description: 'Create a new project. arg: in directory, no arg: in current directory',
            inputParams: [],
            optionalParams: ['projectName']
        },
        CreateNewAction: {
            name: 'new-action',
            description: 'Create a new action',
            inputParams: ['package_name'],
            optionalParams: ['']
        },
        CreateNewFlow: {
            name: 'new',
            description: 'Create a new component with bound reducer, actions, logic in project directory',
            inputParams: ['logicName'],
            optionalParams: []
        },
        UnzipTemplates: {
            name: 'unzip',
            description: 'Unzipping templates',
            inputParams: [],
            optionalParams: ['zip-file']
        }
    },

    hierarchyTypes: {
        Flat: {
            name: 'flat'
        },
        Component: {
            name: 'component'
        }
    },
    printHelp: commands => {
        console.error('-----');
        Object.values(commands).forEach(c => {
            console.log(chalk.grey(`${c.description}`));
            console.log(
                `\t${c.name} [${c.inputParams.join(' ')}] ${
                    c.optionalParams ? `(${c.optionalParams.join(' ')})` : ''
                }`
            );
            console.log('-----');
        });
    }
};
