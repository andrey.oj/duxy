#README

#DO NOT USE YET STILL In TESTING
### **rdxcreate** is created to help you save time in your *React & Redux* project


###Way 1 Add to existing application

Install  
`yarn add rdxcreate`  

Create new flow  
`node node_modules/rdxcreate new-flow my-flow-name`

###Way 2 Create fresh
##### Refer to _https://gitlab.com/andrey.oj/blank-redux-project/blob/master/README.md  

Create new project dir  
`mkdir myproject`

Clone the project  
`git clone git@gitlab.com:andrey.oj/blank-redux-project.git myproject`  

Go into that dir 
`cd myproject`

Install dependencies  
`yarn install`  

Create new flow  
`yarn new-flow {name}`  

